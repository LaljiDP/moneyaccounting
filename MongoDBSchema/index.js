var mongoose = require('mongoose');
const { Schema } = mongoose;
const config = require('../config/configuration');

mongoose.connect(config.mongoURI);

const UserSchema = Schema({
    userID:String,
    firstName:String
});

const User = mongoose.model('moneyAccountingUsers',UserSchema,'moneyAccountingUser');

module.exports = User;