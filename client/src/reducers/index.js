import { combineReducers } from 'redux';
import { Auth } from './Authentication/Auth';

export const store = combineReducers({
    Auth,
});