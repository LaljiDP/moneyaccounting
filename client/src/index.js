import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createStore, applyMiddleware  } from 'redux';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import { store } from './reducers';
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { initialState } from './reducers/InitialState';
import { MuiThemeProvider } from 'material-ui';

const reduxStore = createStore(store,initialState,composeWithDevTools(applyMiddleware(reduxThunk)));

ReactDOM.render(
<MuiThemeProvider>
    <Provider store={reduxStore}>
        <App />
    </Provider>
</MuiThemeProvider>
, document.getElementById('root'));
