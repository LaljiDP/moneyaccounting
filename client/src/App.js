import React, { Component } from 'react';
import { AppBar } from 'material-ui';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './components/Login';
import Registration from './components/Registration';


class App extends Component {

  Error = () => {
    return (
      <div> Not Match 404 </div>
    )
  }

  render() {
    return (
      <div>
         <AppBar
            title="AppBar"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
         />
         <div>
            <BrowserRouter>
                <Switch>
                      <Route exact path="/" component={Login} />
                      <Route exact path="/app/register" component={Registration} />
                      <Route component={this.Error}/>
                </Switch>
            </BrowserRouter>
         </div>
      </div>
    );
  }
}

export default App;