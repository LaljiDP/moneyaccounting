var express = require('express');
var User = require('../../MongoDBSchema/index');
var JWT = require('jsonwebtoken');
var config = require('../../config/configuration');
const router = express.Router();

router.post('/', (req, res) => {
        const { email, password } = req;
        if(email && password){
            res.send({message:'Invalid Request Parameter mismatched.'})
        }else{
            User.find({email:email, password:password}, (err, user)=> {
                if(err){
                    res.send({message:'Error found',err:err})
                }else{
                   const Token = JWT.sign({email:user.email, firstname:user.firstname}, config.secretKey);
                   res.send({...user,Token});
                }
            })            
        }
});